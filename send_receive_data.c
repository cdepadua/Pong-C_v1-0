#include "send_receive_data.h"

void* sent_receive_data(void *myData)
{
    //INIT SOUNDS
    FMOD_SYSTEM *system;
    FMOD_System_Create(&system);
    FMOD_System_Init(system, 2, FMOD_INIT_NORMAL, NULL);
    FMOD_SOUND *hit_sound = NULL;
    FMOD_SOUND *point_sound = NULL;
    FMOD_CHANNELGROUP *channelGroup;
    FMOD_System_GetMasterChannelGroup(system, &channelGroup);
    FMOD_System_CreateSound(system, "sounds/beep-07.wav",FMOD_CREATESAMPLE, 0, &hit_sound);
    FMOD_System_CreateSound(system, "sounds/WOWSENSOR-HIT.WAV",FMOD_CREATESAMPLE, 0, &point_sound);

    t_thread_data *data = myData;
    t_socket_data data_to_send;
    //TEST
    t_thread_data **r_data = &myData;
    t_socket_data received_data;
    //END
    int previus_time = 0, actual_time = 0;
    while (1)
    {
        actual_time = SDL_GetTicks();
        if (actual_time - previus_time > DELAY)
        {
            data_to_send.player_1_y = *data->player_1_y;
            data_to_send.player_2_y = *data->player_2_y;
            if(data->isServer == 1)
            {
                data_to_send.ball_x = *data->ball_x;
                data_to_send.ball_y = *data->ball_y;
                data_to_send.score_p1 = *data->score_p1;
                data_to_send.score_p2 = *data->score_p2;
                data_to_send.signal = *data->signal;
                if(*data->signal == HIT)
                {
                        FMOD_System_PlaySound(system, hit_sound,channelGroup , 0, NULL);
                }
                else if (*data->signal == GET_POINT)
                {
                        FMOD_System_PlaySound(system, point_sound,channelGroup , 0, NULL);
                }

            }
            *(data->sock_err) = send(*(data->sock),&data_to_send,sizeof(data_to_send),0);
            if (*(data->sock_err) == SOCKET_ERROR)
            {
                FMOD_Sound_Release(hit_sound);
                FMOD_Sound_Release(point_sound);
                FMOD_System_Close(system);
                FMOD_System_Release(system);
                printf("error");
                pthread_exit(NULL);
            }


            // TEST  THREAD
            if (recv(*(*r_data)->sock, &received_data,sizeof(received_data),0))
            {
                *(*r_data)->player_2_y = received_data.player_1_y;
                if ((*r_data)->isServer == 0)
                {
                    *(*r_data)->ball_x = received_data.ball_x;
                    *(*r_data)->ball_y = received_data.ball_y;
                    *(*r_data)->score_p1 = received_data.score_p1;
                    *(*r_data)->score_p2 = received_data.score_p2;
                    if(received_data.signal == HIT)
                    {
                        FMOD_System_PlaySound(system, hit_sound,channelGroup , 0, NULL);
                    }
                    else if (received_data.signal == GET_POINT)
                    {
                        *(*r_data)->player_1_y = INITIAL_y_PLAYER;
                        FMOD_System_PlaySound(system, point_sound,channelGroup , 0, NULL);
                    }
                    else if (received_data.signal == END)
                    {
                        FMOD_Sound_Release(hit_sound);
                        FMOD_Sound_Release(point_sound);
                        FMOD_System_Close(system);
                        FMOD_System_Release(system);
                        pthread_exit(NULL);
                    }
                }
            }
            //END TEST
            previus_time = actual_time;
        }
        else
         {
            SDL_Delay(DELAY - (actual_time - previus_time));
         }
    }
    FMOD_Sound_Release(hit_sound);
    FMOD_Sound_Release(point_sound);
    FMOD_System_Close(system);
    FMOD_System_Release(system);
    return (void*)0;
}
