#include "game.h"

int main_game(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores, int isServer, int *signal)
{
    int continuer;
    SDL_Event event;
    continuer = 1;
    int actual_time = 0, previus_time = 0;

    while(continuer)
    {
        SDL_PollEvent(&event);
        switch( event.type ){
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        *signal = END;
                        continuer = 0;
                        break;
                    case SDLK_UP:
                        (*player_1)->direction = UP;
                        collision_test_for_player(player_1);
                        break;
                    case SDLK_DOWN:
                        (*player_1)->direction = DOWN;
                        collision_test_for_player(player_1);
                        break;
                    default :
                        break;
                }
                break;
            case SDL_KEYUP:
                (*player_1)->direction = STOP;
                break;
            case SDL_QUIT:
                *signal = END;
                continuer = 0;
                break;
            default:
                break;
        }
        //vide l'�cran
        SDL_FillRect(*ecran, NULL, SDL_MapRGB((*ecran)->format, 0, 0, 0));

        //bouge la balle
        actual_time = SDL_GetTicks();
        if (actual_time - previus_time > DELAY)
        {
            if(isServer == 1)
            {
                move_ball(ecran, player_1, player_2, ball, scores, signal);
            }
            previus_time = actual_time;
        }
        else
         {
            SDL_Delay(DELAY - (actual_time - previus_time));
         }

        blitt_all_surfaces(ecran,player_1,player_2,ball,scores);

        SDL_Flip(*ecran);

        //verifie la fin de la partie
        if(
           ((*player_1)->score >= 5 && isServer) ||
           ((*player_2)->score >= 5 && !isServer)
           )
        {
            *signal = END;
            end_game(ecran, 1);
            continuer = 0;
            return 0;
        }
        else if(
           ((*player_2)->score >= 5 && isServer) ||
           ((*player_1)->score >= 5 && !isServer)
           )
        {
            *signal = END;
            end_game(ecran, 0);
            continuer = 0;
            return 0;
        }
    }
    TTF_CloseFont((*scores)->font);
    SDL_FreeSurface((*player_1)->rectangle);
    SDL_FreeSurface((*player_2)->rectangle);
    SDL_FreeSurface((*ball)->rectangle);
    SDL_FreeSurface((*scores)->score_p1);
    SDL_FreeSurface((*scores)->score_p2);
    return (0);
}

void collision_test_for_player(t_player **player_1)
{
    if ((*player_1)->direction == UP)
    {
        if ((*player_1)->position.y > MARGIN)
        {
            (*player_1)->position.y--;
        }
    }
    else if ((*player_1)->direction == DOWN)
    {
        if ((*player_1)->position.y < SCREEN_H - PLAYER_H - MARGIN)
        {
            (*player_1)->position.y++;
        }
    }
}

void move_ball(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores, int *signal)
{
    //collision avec j1
    *signal = NONE;
    if (
            (
                (*ball)->position.x <= ((*player_1)->position.x + PLAYER_W) &&
                (
                 ((*ball)->position.y + BALL_H) > (*player_1)->position.y &&
                 (*ball)->position.y < ((*player_1)->position.y + PLAYER_H)
                )
            )

        )
    {
        *signal = HIT;
        change_ball_angle(ball, player_1);
        (*ball)->direction = (*ball)->direction * (-1);
    }
    // collision avec j2
    else if(
                ((*ball)->position.x + BALL_W) >= (*player_2)->position.x &&
                (
                 ((*ball)->position.y + BALL_H) > (*player_2)->position.y &&
                 (*ball)->position.y < ((*player_2)->position.y + PLAYER_H)
                )
            )
    {
        *signal = HIT;
        change_ball_angle(ball, player_2);
        (*ball)->direction = (*ball)->direction * (-1);
    }
    else
    {
        //collision avec les bords haut et bas
        if ((*ball)->position.y <= 0 || (*ball)->position.y + BALL_H >= SCREEN_H)
        {
            *signal = HIT;
            (*ball)->angle = (*ball)->angle * (-1);
        }

        //collision avec le bord gauche
        if ((*ball)->position.x <= 0)
        {
            *signal = GET_POINT;
            (*player_2)->score ++;
            restart_ball(ecran,player_1,player_2,ball, scores);
        }
        //collision avec le bord droite
        if ((*ball)->position.x + BALL_W >= SCREEN_W)
        {
            *signal = GET_POINT;
            (*player_1)->score ++;
            restart_ball(ecran,player_1,player_2,ball, scores);
        }
    }
    // mouvement de la balle
    (*ball)->position.x += (*ball)->direction;
    (*ball)->position.y += (*ball)->angle;
}

void change_ball_angle(t_ball **ball, t_player **player)
{
    if(
        (*ball)->position.y + BALL_H >= (*player)->position.y &&
        (*ball)->position.y + BALL_H <= (*player)->position.y + (PLAYER_H / 4)
       )
   {
       (*ball)->angle = -2;
   }
   else if(
            (*ball)->position.y + BALL_H >= (*player)->position.y + (PLAYER_H / 4)&&
            (*ball)->position.y + BALL_H <= (*player)->position.y + (PLAYER_H / 2)
           )
   {
       (*ball)->angle = -1;
   }
   else if(
            (*ball)->position.y >= (*player)->position.y + (PLAYER_H / 2) &&
            (*ball)->position.y <=  (*player)->position.y + ((PLAYER_H / 4) * 3)
           )
   {
       (*ball)->angle = 1;
   }
   else if(
            (*ball)->position.y >=  (*player)->position.y + ((PLAYER_H / 4) * 3) &&
            (*ball)->position.y <= (*player)->position.y + PLAYER_H
           )
   {
       (*ball)->angle = 2;
   }
   else
   {
       (*ball)->angle = 0;
   }
}

void restart_ball(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores)
{
    (*ball)->angle = 0;
    (*ball)->position.x = INITIAL_X_BALL;
    (*ball)->position.y = INITIAL_Y_BALL;

    (*player_1)->position.y = INITIAL_y_PLAYER;
    (*player_2)->position.y = INITIAL_y_PLAYER;

    //vide l'�cran
    SDL_FillRect(*ecran, NULL, SDL_MapRGB((*ecran)->format, 0, 0, 0));

    blitt_all_surfaces(ecran,player_1,player_2,ball,scores);

    SDL_Flip(*ecran);
}
