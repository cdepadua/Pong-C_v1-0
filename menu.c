#include "menu.h"

int openMenu(SDL_Surface **screen)
{
    int selection;
    if (!screen)
    {
        fprintf(stderr,"Erreur de cr�ation de la fen�tre: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }
    //vide l'�cran
    SDL_FillRect(*screen, NULL, SDL_MapRGB((*screen)->format, 0, 0, 0));
    TTF_Font *font;
    SDL_Surface *title;
    SDL_Rect position;
    SDL_Color color = {255, 255, 255};
    font = TTF_OpenFont("fonts/ARCADECLASSIC.TTF", 120);
    title = TTF_RenderText_Blended(font, "PONG", color);
    position.x = (SCREEN_W / 2) - (title->w / 2);
    position.y = MARGIN;
    SDL_BlitSurface(title, NULL, *screen, &position);
    draw_menu_text(screen);
    SDL_Flip(*screen);
    selection = menu_selection(*screen);
    SDL_FreeSurface(title);
    TTF_CloseFont(font);
    return selection;
}

int menu_selection()
{
    SDL_Event event;
    int choise = -1;
    int in_menu = 1;
    while(in_menu)
    {
        SDL_WaitEvent(&event);
        switch( event.type )
        {
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_3:
                    case SDLK_KP3:
                    case SDLK_ESCAPE:
                        choise = -1;
                        in_menu = 0;
                        break;
                    case SDLK_1:
                    case SDLK_KP1:
                        choise = 0;
                        in_menu = 0;
                        break;
                    case SDLK_2:
                    case SDLK_KP2:
                        choise = 1;
                        in_menu = 0;
                        break;
                    default :
                        break;
                }
                break;
            case SDL_QUIT:
                choise = -1;
                in_menu = 0;
                break;
            default:
                break;
        }
    }
    return choise;
}

void draw_menu_text(SDL_Surface **screen)
{
    TTF_Font *font;
    SDL_Surface *connection;
    SDL_Surface *hosting;
    SDL_Surface *quit;
    SDL_Rect position;

    SDL_Color color = {255, 255, 255};
    font = TTF_OpenFont("fonts/ARCADECLASSIC.TTF", 30);
    connection = TTF_RenderText_Blended(font, "1    Se  connecter    p2", color);
    hosting = TTF_RenderText_Blended(font, "2    Heberger    p1", color);
    quit = TTF_RenderText_Blended(font, "3    Quitter", color);
    position.x = (SCREEN_W / 2) - (connection->w / 2);
    position.y = SCREEN_H / 2;
    SDL_BlitSurface(connection, NULL, *screen, &position);
    position.x = (SCREEN_W / 2) - (hosting->w / 2);
    position.y += connection->h + 5;
    SDL_BlitSurface(hosting, NULL, *screen, &position);
    position.x = (SCREEN_W / 2) - (quit->w / 2);
    position.y += hosting->h + 5;;
    SDL_BlitSurface(quit, NULL, *screen, &position);
}
