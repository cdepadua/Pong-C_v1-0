#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "server_socket.h"
#include "client_socket.h"


int openMenu(SDL_Surface **screen);
int menu_selection();
void draw_menu_text(SDL_Surface **screen);

#endif // MENU_H_INCLUDED
