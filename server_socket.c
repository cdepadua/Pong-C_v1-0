#include "server_socket.h"

void launchServer(SDL_Surface **ecran, FMOD_SYSTEM **system, FMOD_SOUND **music, FMOD_CHANNELGROUP **channelGroup)
{
    /* socket et contexte d'adressage du serveur */
    SOCKADDR_IN sin;
    SOCKET sock; //Pour utiliser une socket on d�clare une variable de type SOCKET
    socklen_t recsize = sizeof(sin);

    /* Socket et context addresage client */
    SOCKADDR_IN csin;
    SOCKET csock;
    socklen_t crecsize = sizeof(csin);

    //threads
    pthread_t send_thread;

    int sock_err;

    SDL_FillRect(*ecran, NULL, SDL_MapRGB((*ecran)->format, 0, 0, 0));
    SDL_Flip(*ecran);

    // cr�ation d'une socket
    sock = socket(AF_INET,SOCK_STREAM,0);
    if (sock != INVALID_SOCKET)
    {
        // Configuration
        sin.sin_addr.s_addr = htonl(INADDR_ANY);  /* Adresse IP automatique */
        sin.sin_family = AF_INET;                 /* Protocole familial (IP) */
        sin.sin_port = htons(PORT);               /* Listage du port */
        sock_err = bind(sock, (SOCKADDR*)&sin, recsize);
        if (sock_err != SOCKET_ERROR)
        {
            /* D�marrage du listage (mode server) */
            sock_err = listen(sock, 5);
            if (sock_err != SOCKET_ERROR)
            {
               /* Attente pendant laquelle le client se connecte */
                whrite_text_on_screen(ecran, "EN   ATTENTE   D   UN   JOUEUR");

                csock = accept(sock, (SOCKADDR*)&csin, &crecsize);
                //envoie des donn�es

                //init Player 1
                t_player *player_1 = NULL;
                player_1 = malloc(sizeof(t_player));
                if (player_1 == NULL)
                    exit(EXIT_FAILURE);

                //init Player 2
                t_player *player_2 = NULL;
                player_2 = malloc(sizeof(t_player));
                if (player_2 == NULL)
                    exit(EXIT_FAILURE);

                //init ball
                t_ball  *ball = NULL;
                ball = malloc(sizeof(t_ball));
                if (ball == NULL)
                    exit(EXIT_FAILURE);

                //init score display
                t_score_display *scores = NULL;
                scores = malloc(sizeof(t_score_display));
                if (scores == NULL)
                    exit(EXIT_FAILURE);

                //init signal
                int signal = NONE;

                //Stop sound
                FMOD_ChannelGroup_Stop(*channelGroup);
                FMOD_Sound_Release(*music);
                FMOD_System_Close(*system);
                FMOD_System_Release(*system);

                init_game(ecran, &player_1, &player_2, &ball, &scores, 1);

                //TODO: send data
                t_thread_data *data_for_sent = init_data_for_comunication(&csock,&sock_err,&player_1,&player_2,&ball,1,&signal);

                pthread_create(&send_thread, NULL, sent_receive_data, (void*)data_for_sent);

                main_game(ecran,&player_1, &player_2, &ball, &scores, 1, &signal);

                free(player_1);
                free(player_2);
                free(ball);
                free(scores);
                free(data_for_sent);

                //ferme la connexion
                shutdown(csock,2);
            }
            else
            {
                perror("listen");
            }
        }
        else
        {
            perror("bind");
        }
        /* Fermeture de la socket client et de la socket serveur */
        printf("Fermeture de la socket client\n");
        closesocket(csock);
        printf("Fermeture de la socket serveur\n");
        closesocket(sock);
        printf("Fermeture du serveur termin�e\n");
    }
    else
    {
        perror("scket");
    }
}
