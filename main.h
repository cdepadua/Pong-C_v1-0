#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "server_socket.h"
#include "client_socket.h"
#include "menu.h"

#endif // MAIN_H_INCLUDED
