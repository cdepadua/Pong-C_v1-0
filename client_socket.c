#include "client_socket.h"

void launchClient(SDL_Surface **ecran, FMOD_SYSTEM **system, FMOD_SOUND **music, FMOD_CHANNELGROUP **channelGroup)
{
    SOCKET sock;
    SOCKADDR_IN sin;
    int sock_err;
    int connected = 0;
    int try_connect = 1;
    //threads
    pthread_t send_thread;

    SDL_FillRect(*ecran, NULL, SDL_MapRGB((*ecran)->format, 0, 0, 0));
    SDL_Flip(*ecran);

    // recuperation donn�es del'hote
    struct hostent *hostinfo = NULL;
    char *hostname = NULL;

    while(try_connect)
    {
        hostname = read_ip (ecran);
        hostinfo = gethostbyname(hostname);
        if (hostinfo == NULL) /* l'h�te n'existe pas */
        {
            whrite_text_on_screen(ecran, "SERVEUR   INCONNU");
            SDL_Delay(DELAY_AFTER_POINT);
        }
        else
            try_connect = 0;
    }

    /* Configuration de la connexion */
    sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);

    /* Cr�ation de la socket */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    /* Si le client arrive � se connecter */

    whrite_text_on_screen(ecran, "VEUILLEZ   SAISIR   L   ADRESSE   IP   LOCALE   D   UN   SERVEUR");
    while (!connected)
    {
        if(connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != SOCKET_ERROR)
        {
            connected = 1;
            //printf("Connexion � %s sur le port %d\n", inet_ntoa(sin.sin_addr), htons(sin.sin_port));

            //init Player 1
            t_player *player_1 = NULL;
            player_1 = malloc(sizeof(t_player));
            if (player_1 == NULL)
                exit(EXIT_FAILURE);

            //init Player 2
            t_player *player_2 = NULL;
            player_2 = malloc(sizeof(t_player));
            if (player_2 == NULL)
                exit(EXIT_FAILURE);

            //init ball
            t_ball  *ball = NULL;
            ball = malloc(sizeof(t_ball));
            if (ball == NULL)
                exit(EXIT_FAILURE);

            //init score display
            t_score_display *scores = NULL;
            scores = malloc(sizeof(t_score_display));
            if (scores == NULL)
                exit(EXIT_FAILURE);

            //Stop sound
            FMOD_ChannelGroup_Stop(*channelGroup);
            FMOD_Sound_Release(*music);
            FMOD_System_Close(*system);
            FMOD_System_Release(*system);
            init_game(ecran, &player_1, &player_2, &ball, &scores, 0);

            //init signal
            int signal = NONE;

            //reception des donn�es
            t_thread_data *data_for_sent = init_data_for_comunication(&sock,&sock_err,&player_1,&player_2,&ball,0, &signal);

            pthread_create(&send_thread, NULL, sent_receive_data, (void*)data_for_sent);

            main_game(ecran,&player_1, &player_2, &ball, &scores, 0, &signal);

            free(player_1);
            free(player_2);
            free(ball);
            free(scores);
            free(data_for_sent);

        }
    }
    /* On ferme la socket pr�c�demment ouverte */
    closesocket(sock);
}
