#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "structs.h"
#include <fmod.h>

void init_game(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores, int is_server);
t_thread_data *init_data_for_comunication (SOCKET *sock, int *sock_err, t_player **player_1, t_player **player_2, t_ball **ball, int is_server, int *signal);
void blitt_all_surfaces (SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores);
void whrite_text_on_screen(SDL_Surface **ecran, char text_to_whrite[255]);
char *read_ip (SDL_Surface **ecran);
int end_game(SDL_Surface **screen, int win);

#endif // TOOLS_H_INCLUDED
