#include "tools.h"

void init_game(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores, int is_server)
{
    (*player_1)->rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, PLAYER_W, PLAYER_H, 32, 0, 0, 0, 0);
    if (is_server == 1)
        (*player_1)->position.x = MARGIN;
    else
        (*player_1)->position.x = INITIAL_X_PLAYER;
    (*player_1)->position.y = INITIAL_y_PLAYER;
    (*player_1)->score = 0;

    (*player_2)->rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, PLAYER_W, PLAYER_H, 32, 0, 0, 0, 0);
    if (is_server == 1)
        (*player_2)->position.x = INITIAL_X_PLAYER;
    else
        (*player_2)->position.x = MARGIN;
    (*player_2)->position.y = INITIAL_y_PLAYER;
    (*player_2)->score = 0;

    (*ball)->rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, BALL_W, BALL_H, 32, 0, 0, 0, 0);
    (*ball)->position.x = INITIAL_X_BALL;
    (*ball)->position.y = INITIAL_Y_BALL;
    (*ball)->angle = 0;
    (*ball)->direction = 2;

    (*scores)->font = TTF_OpenFont("fonts/ARCADECLASSIC.TTF", 30);
    sprintf((*scores)->text,"P0     00");
    (*scores)->score_p1 = TTF_RenderText_Solid((*scores)->font, (*scores)->text, (*scores)->color);
    (*scores)->score_p2 = TTF_RenderText_Solid((*scores)->font, (*scores)->text, (*scores)->color);
    (*scores)->color.r = 255;
    (*scores)->color.g = 255;
    (*scores)->color.b = 255;
    (*scores)->score_p1_position.x = (SCREEN_W / 4) - ((*scores)->score_p1->w / 2);
    (*scores)->score_p1_position.y = MARGIN;
    (*scores)->score_p2_position.x = ((SCREEN_W / 4) * 3) - ((*scores)->score_p2->w / 2);
    (*scores)->score_p2_position.y = MARGIN;
}

t_thread_data *init_data_for_comunication (SOCKET *sock, int *sock_err, t_player **player_1, t_player **player_2, t_ball **ball, int is_server, int *signal)
{
    t_thread_data *data_for_sent = NULL;
    data_for_sent = malloc(sizeof(t_thread_data));
    if (data_for_sent == NULL)
        exit(EXIT_FAILURE);
    data_for_sent->sock = sock;
    data_for_sent->sock_err = sock_err;
    data_for_sent->player_1_y = &(*player_1)->position.y;
    data_for_sent->player_2_y = &(*player_2)->position.y;
    data_for_sent->isServer = is_server;
    data_for_sent->ball_x = &(*ball)->position.x;
    data_for_sent->ball_y = &(*ball)->position.y;
    data_for_sent->score_p1 = &(*player_1)->score;
    data_for_sent->score_p2 = &(*player_2)->score;
    data_for_sent->signal = signal;

    return data_for_sent;
}

void blitt_all_surfaces (SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores)
{
    //Affichage player 1
    SDL_FillRect((*player_1)->rectangle, NULL, SDL_MapRGB((*ecran)->format, 255, 255, 255));
    SDL_BlitSurface((*player_1)->rectangle, NULL, *ecran, &((*player_1)->position));

    //Affichage player 2
    SDL_FillRect((*player_2)->rectangle, NULL, SDL_MapRGB((*ecran)->format, 255, 255, 255));
    SDL_BlitSurface((*player_2)->rectangle, NULL, *ecran, &((*player_2)->position));

    //Affichage balle
    SDL_FillRect((*ball)->rectangle, NULL, SDL_MapRGB((*ecran)->format, 255, 255, 255));
    SDL_BlitSurface((*ball)->rectangle, NULL, *ecran, &((*ball)->position));

    //Affichage score
    sprintf((*scores)->text,"P1     %i",(*player_1)->score );
    (*scores)->score_p1 = TTF_RenderText_Solid((*scores)->font, (*scores)->text, (*scores)->color);
    sprintf((*scores)->text,"P2     %i",(*player_2)->score );
    (*scores)->score_p2 = TTF_RenderText_Solid((*scores)->font, (*scores)->text, (*scores)->color);
    SDL_BlitSurface((*scores)->score_p1, NULL, *ecran, &(*scores)->score_p1_position);
    SDL_BlitSurface((*scores)->score_p2, NULL, *ecran, &(*scores)->score_p2_position);
    SDL_FreeSurface((*scores)->score_p1);
    SDL_FreeSurface((*scores)->score_p2);
}

void whrite_text_on_screen(SDL_Surface **ecran, char text_to_whrite[255])
{
    TTF_Font *font;
    SDL_Surface *text;
    SDL_Rect position;
    SDL_Color color = {255, 255, 255};
    char container_text[255];
    sprintf(container_text,"%s",text_to_whrite);
    font = TTF_OpenFont("fonts/ARCADECLASSIC.TTF", 17);
    text = TTF_RenderText_Blended(font, container_text, color);
    position.x = MARGIN;
    position.y = MARGIN;
    //vide l'�cran
    SDL_FillRect(*ecran, NULL, SDL_MapRGB((*ecran)->format, 0, 0, 0));
    SDL_BlitSurface(text, NULL, *ecran, &position);
    SDL_Flip(*ecran);
    SDL_FreeSurface(text);
    TTF_CloseFont(font);
}

char *read_ip (SDL_Surface **ecran)
{
    TTF_Font *font;
    SDL_Surface *text;
    SDL_Rect position;
    SDL_Color color = {255, 255, 255};
    int continuer = 1;
    SDL_Event event;
    char text_to_return[255] = "";
    int i = 0;
    font = TTF_OpenFont("fonts/ARCADECLASSIC.TTF", 17);
    position.x = MARGIN;
    position.y = MARGIN * 8;
    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch( event.type ){
            case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
                case SDLK_ESCAPE:
                    continuer = 0;
                    break;
                case SDLK_0:
                case SDLK_KP0:
                    text_to_return[i] = '0';
                    i++;
                    break;
                case SDLK_1:
                case SDLK_KP1:
                    text_to_return[i] = '1';
                    i++;
                    break;
                case SDLK_2:
                case SDLK_KP2:
                    text_to_return[i] = '2';
                    i++;
                    break;
                case SDLK_3:
                case SDLK_KP3:
                    text_to_return[i] = '3';
                    i++;
                    break;
                case SDLK_4:
                case SDLK_KP4:
                    text_to_return[i] = '4';
                    i++;
                    break;
                case SDLK_5:
                case SDLK_KP5:
                    text_to_return[i] = '5';
                    i++;
                    break;
                case SDLK_6:
                case SDLK_KP6:
                    text_to_return[i] = '6';
                    i++;
                    break;
                case SDLK_7:
                case SDLK_KP7:
                    text_to_return[i] = '7';
                    i++;
                    break;
                case SDLK_8:
                case SDLK_KP8:
                    text_to_return[i] = '8';
                    i++;
                    break;
                case SDLK_9:
                case SDLK_KP9:
                    text_to_return[i] = '9';
                    i++;
                    break;
                case SDLK_PERIOD:
                case SDLK_KP_PERIOD:
                    text_to_return[i] = '.';
                    i++;
                    break;
                case SDLK_BACKSPACE:
                    i--;
                    text_to_return[i] = ' ';
                    break;
                case SDLK_RETURN:
                case SDLK_KP_ENTER:
                    text_to_return[i] = '\0';
                    continuer = 0;
                default:
                    break;
            }
        }
        whrite_text_on_screen(ecran, "VEUILLEZ   SAISIR   L   ADRESSE   IP   LOCALE   D   UN   SERVEUR");
        text = TTF_RenderText_Blended(font, text_to_return, color);
        SDL_BlitSurface(text, NULL, *ecran, &position);
        SDL_Flip(*ecran);
        SDL_FreeSurface(text);
    }
    char *tmp = NULL;
    tmp = malloc(sizeof(char)*255);
    if (tmp == NULL)
        exit(EXIT_FAILURE);
    sprintf(tmp,text_to_return);
    TTF_CloseFont(font);
    return tmp;
}

int end_game(SDL_Surface **screen, int win)
{
    if (!screen)
    {
        fprintf(stderr,"Erreur de cr�ation de la fen�tre: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_Event event;
    int whait = 1;
    TTF_Font *font;
    SDL_Surface *title;
    SDL_Rect position;
    SDL_Color color = {255, 255, 255};
    font = TTF_OpenFont("fonts/ARCADECLASSIC.TTF", 110);
    //Play Sound
    FMOD_SYSTEM *system;
    FMOD_System_Create(&system);
    FMOD_System_Init(system, 2, FMOD_INIT_NORMAL, NULL);
    FMOD_SOUND *music = NULL;
    FMOD_CHANNELGROUP *channelGroup;
    FMOD_System_GetMasterChannelGroup(system, &channelGroup);
    if(win)
    {
        FMOD_System_CreateSound(system, "sounds/win.mp3",FMOD_CREATESAMPLE, 0, &music);
    }
    else
    {
        FMOD_System_CreateSound(system, "sounds/game_over.mp3",FMOD_CREATESAMPLE, 0, &music);
    }

    FMOD_System_PlaySound(system, music,channelGroup , 0, NULL);
    if (win)
    {
        title = TTF_RenderText_Blended(font, "YOU    WIN", color);
    }
    else
    {
        title = TTF_RenderText_Blended(font, "GAME   OVER", color);
    }
    position.x = (SCREEN_W / 2) - (title->w / 2);
    position.y = (SCREEN_H / 2) - (title->h / 2);
    //vide l'�cran
    SDL_FillRect(*screen, NULL, SDL_MapRGB((*screen)->format, 0, 0, 0));
    SDL_BlitSurface(title, NULL, *screen, &position);
    SDL_Flip(*screen);

    while (whait)
    {
        SDL_PollEvent(&event);
        switch( event.type ){
            case SDL_KEYDOWN:
                whait = 0;
                break;
            default:
                break;
        }
    }
    SDL_FreeSurface(title);
    TTF_CloseFont(font);
    FMOD_Sound_Release(music);
    FMOD_System_Close(system);
    FMOD_System_Release(system);
    return 0;
}
