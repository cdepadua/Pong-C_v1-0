#include "main.h"

typedef int socklen_t;

int main(int argc, char *argv[])
{
    freopen("CON", "w", stdout);  //ligne a supprimer: �a ser juste a impecher sdl de rediriger printf
    //Code Obligatoire � ajouter au d�but de la fonction main;
    WSADATA WSAData;
    int error;
    int in_game = 1;
    error = WSAStartup(MAKEWORD(2,2), &WSAData);//initialise la biblioteque WinSock
    //La Macro MAKEWORD transforme les deux entiers qui lui sont pass�s en param�tres en un seul entier (de 2 octets) qu'elle retourne
    //SDL
    if (SDL_Init(SDL_INIT_VIDEO) == -1) // D�marrage de la SDL. Si erreur :
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); // �criture de l'erreur
        exit(EXIT_FAILURE); // On quitte le programme
    }
    if(TTF_Init() == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_Surface *ecran = NULL;
    ecran = SDL_SetVideoMode(SCREEN_W, SCREEN_H, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);

    if (ecran == NULL) // Si l'ouverture a �chou�, on le note et on arr�te
    {
        fprintf(stderr, "Impossible de charger le mode vid�o : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_WM_SetCaption("Pong", NULL);

    while (in_game)
    {
        int menu_choice;
        //Play Sound
        FMOD_SYSTEM *system;
        FMOD_System_Create(&system);
        FMOD_System_Init(system, 2, FMOD_INIT_NORMAL, NULL);
        FMOD_SOUND *music = NULL;
        FMOD_CHANNELGROUP *channelGroup;
        FMOD_System_GetMasterChannelGroup(system, &channelGroup);
        FMOD_System_CreateSound(system, "sounds/03 Chibi Ninja.mp3",FMOD_2D | FMOD_CREATESTREAM, 0, &music);
        FMOD_System_PlaySound(system, music,channelGroup , 0, NULL);
        FMOD_Sound_SetLoopCount(music, -1);

        menu_choice = openMenu(&ecran);

        if (!error)
        {

                if(menu_choice == 0)
                    launchClient(&ecran,&system,&music,&channelGroup);
                else if(menu_choice == 1)
                    launchServer(&ecran,&system,&music,&channelGroup);
                else if(menu_choice == -1)
                    in_game = 0;
        }
    }

    SDL_FreeSurface(ecran);
    WSACleanup();
    TTF_Quit();
    SDL_Quit();
    return EXIT_SUCCESS;
}
