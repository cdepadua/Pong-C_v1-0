#ifndef SEND_RECEIVE_DATA_H_INCLUDED
#define SEND_RECEIVE_DATA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <pthread.h>
#include "structs.h"
#include <fmod.h>

void* sent_receive_data(void *data);

#endif // SEND_RECEIVE_DATA_H_INCLUDED
