#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#define DELAY               5
#define MARGIN              5
#define DELAY_AFTER_POINT   2000
#define PORT                23
#define SCREEN_W            640
#define SCREEN_H            480
#define PLAYER_H            100
#define PLAYER_W            20
#define BALL_H              20
#define BALL_W              20
#define MAX_SCORE           5
#define INITIAL_X_PLAYER    SCREEN_W - PLAYER_W - MARGIN
#define INITIAL_y_PLAYER    (SCREEN_H / 2) - (PLAYER_H / 2)
#define INITIAL_X_BALL      (SCREEN_W / 2) - (BALL_W / 2)
#define INITIAL_Y_BALL      (SCREEN_H / 2) - (BALL_H / 2)

typedef int socklen_t;

enum Direction {UP, DOWN, STOP};

enum Signals {GET_POINT,HIT,END,NONE};

typedef struct      s_player
{
    SDL_Surface     *rectangle;
    SDL_Rect        position;
    int             direction;
    int             score;
    int             signal;
}                   t_player;

typedef struct      s_ball
{
    SDL_Surface     *rectangle;
    SDL_Rect        position;
    float           direction;
    Sint16          angle;
}                   t_ball;

typedef struct      s_score_display
{
    SDL_Surface     *score_p1;
    SDL_Surface     *score_p2;
    SDL_Rect        score_p1_position;
    SDL_Rect        score_p2_position;
    SDL_Color       color;
    char            text[10];
    TTF_Font        *font;
}                   t_score_display;

typedef struct      s_socket_data
{
    Sint16          player_1_y;
    Sint16          player_2_y;
    Sint16          ball_x;
    Sint16          ball_y;
    int             score_p1;
    int             score_p2;
    int             signal;
}                   t_socket_data;

typedef struct      s_thread_data
{
    SOCKET          *sock;
    int             *sock_err;
    int             isServer;
    Sint16          *player_1_y;
    Sint16          *player_2_y;
    Sint16          *ball_x;
    Sint16          *ball_y;
    int             *score_p1;
    int             *score_p2;
    int             *signal;
}                   t_thread_data;

#endif // STRUCTS_H_INCLUDED
