#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "structs.h"
#include "tools.h"

int main_game(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores, int isServer, int *signal);
void collision_test_for_player(t_player **player_1);
void move_ball(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores, int *signal);
void restart_ball(SDL_Surface **ecran, t_player **player_1, t_player **player_2, t_ball **ball, t_score_display **scores);
void change_ball_angle(t_ball **ball, t_player **player);

#endif // GAME_H_INCLUDED
