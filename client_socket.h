#ifndef CLIENT_SOCKET_H_INCLUDED
#define CLIENT_SOCKET_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <pthread.h>
#include <SDL/SDL.h>
#include "send_receive_data.h"
#include "structs.h"
#include "game.h"
#include "tools.h"

void launchClient(SDL_Surface **ecran, FMOD_SYSTEM **system, FMOD_SOUND **music, FMOD_CHANNELGROUP **channelGroup);

#endif // CLIENT_SOCKET_H_INCLUDED
